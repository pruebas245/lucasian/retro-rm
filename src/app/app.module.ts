import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from "./components/shared/material.module";
import {PaginationComponent} from './components/pagination/pagination.component';
import {CharacterComponent} from './components/character/character.component';

@NgModule({
  declarations: [
    AppComponent,
    PaginationComponent,
    CharacterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
