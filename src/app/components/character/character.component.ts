import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InfoModel} from "../../models/info.model";
import {CharactersService} from "../../services/characters/characters.service";

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  public character!: InfoModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: CharactersService
  ) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.getInfo(parseInt(id ? id : '1'));
  }

  getInfo(id: number) {
    this.service.show(id).subscribe(
      (response) => {
        this.character = response;
      }
    );
  }

  return() {
    this.router.navigate(['/characters']);
  }

}
