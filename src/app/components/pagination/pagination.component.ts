import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CharactersService} from "../../services/characters/characters.service";
import {CharactersModel} from "../../models/characters.model";
import {InfoModel} from "../../models/info.model";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  public data!: CharactersModel;

  constructor(
    private router: Router,
    private service: CharactersService
  ) {
  }

  ngOnInit(): void {
    this.pagination(1);
  }

  pagination(page: number) {
    this.service.page(page).subscribe(
      (response) => {
        if (response) {
          this.data = response;
        }
      }
    );
  }

  changePageHandler($event: any){
    this.pagination($event.pageIndex + 1);
  }

  showDetail(character: InfoModel) {
    this.router.navigate(['/character', character.id]);
  }

}
