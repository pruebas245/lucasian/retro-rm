export class InfoModel{
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender:string;
  origin: OriginModel;
  location: LocationModel;
  image: string;
  episode: string[];
  url: string;
  created: string;
}

export interface OriginModel{
  name: string;
  url: string;
}

export interface LocationModel{
  name: string;
  url: string;
}
