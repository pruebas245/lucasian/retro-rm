import {PaginationModel} from "./pagination.model";
import {InfoModel} from "./info.model";


export class CharactersModel{
  info: PaginationModel;
  results: InfoModel[];
}
