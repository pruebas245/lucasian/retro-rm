export class PaginationModel{
  count: number;
  pages: number;
  next: string;
  prev: string;
}
