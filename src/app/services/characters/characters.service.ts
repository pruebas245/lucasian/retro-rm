import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CharactersModel} from "../../models/characters.model";
import {InfoModel} from "../../models/info.model";

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  private api = 'https://rickandmortyapi.com/api/character';

  constructor(
    private http: HttpClient
  ) {
  }

  page(page: number): Observable<CharactersModel> {
    const url = `${this.api}/?page=${page}`;
    return this.http.get<CharactersModel>(url);
  }

  show(id: number): Observable<InfoModel> {
    const url = `${this.api}/${id}`;
    return this.http.get<InfoModel>(url);
  }

  filter(name: string): Observable<InfoModel> {
    const url = `${this.api}/?name=${name}`;
    return this.http.get<InfoModel>(url)
  }
}
