import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PaginationComponent} from "./components/pagination/pagination.component";
import {CharacterComponent} from "./components/character/character.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'characters'},
  {path: 'characters', component: PaginationComponent},
  {path: 'character/:id', component: CharacterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
